public class Student {
    private String fio;
    private String groupNum;
    private double[] usp;
    public Student(String fio, String groupNum, double[] uspeva){
        this.fio = fio;
        this.groupNum = groupNum;
        this.usp = uspeva;
    }
    public String getFio(){
        return fio;
    }
    public void setFio(String fio){
        this.fio = fio;
    }
    public String getGroupNum(){
        return groupNum;
    }
    public void setGroupNum(String groupNum){
        this.groupNum = groupNum;
    }
    public double[] getUsp(){
        return usp;
    }
    public void setUsp(double[] usp){
        this.usp = usp;
    }
    public double getSrBall() {
        double sum = 0;
        int k = 0;
        for (int i = 0; i < 5; i++) {
            sum += usp[i];
            k++;
        }
        return sum / k;
    }
    @Override
    public String toString() {
        return String.format("ФИО: %s Группа: %s Успеваемость: %f, %f, %f, %f, %f Средний балл, %f",fio,groupNum, usp[0], usp[1], usp[2], usp[3], usp[4],getSrBall());
    }
}
