import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Book> books = new ArrayList<>();
        Book b = null;
        Library l = new Library(books);
        while(true){
            if(b==null){
                System.out.println("Книга не выбрана. Выберите действие: 1 - добавить книгу. 2 - найти книгу в библиотеке");
                int k = scan.nextInt();
                switch(k){
                    case 1:
                        b = new Book(typeValue("Наименование книги: "),Integer.parseInt(typeValue("Год издания")),typeValue("Автор книги"));
                        l.addBook(b);
                        break;
                    case 2:
                        System.out.println("Выберите тип поиска: 1 - поиск по автору, 2 - поиск по названию, 3 - поиск по году издания");
                        int k1 = scan.nextInt();
                        switch(k1){
                            case 1:
                                b = new Book(l.searchBook(typeValue("Автор книги: "),2).getName(),l.searchBook(typeValue("Автор книги: "),2).getYear(),l.searchBook(typeValue("Автор книги: "),2).getAuthor());
                                break;
                            case 2:
                                b = new Book(l.searchBook(typeValue("Наименование книги"),1).getName(),l.searchBook(typeValue("Наименование книги"),1).getYear(),l.searchBook(typeValue("Наименование книги"),1).getAuthor());
                                break;
                            case 3:
                                b = new Book
                                        (l.searchBook(Integer.parseInt(typeValue("Год издания"))).getName(),l.searchBook(Integer.parseInt(typeValue("Год издания"))).getYear(),l.searchBook(Integer.parseInt(typeValue("Год издания"))).getAuthor());
                                break;
                        }
                }
            }else{
                System.out.println("Выбрана следующая книга:\n"+b.toString()+"\nВыберите действие: 1 - добавить книгу, 2 - найти книгу в библиотеке, 3 - удалить книгу");
                int k = scan.nextInt();
                switch(k){
                    case 1:
                        b = new Book(typeValue("Наименование книги: "),Integer.parseInt(typeValue("Год издания")),typeValue("Автор книги"));
                        l.addBook(b);
                        break;
                    case 2:
                        System.out.println("Выберите тип поиска: 1 - поиск по автору, 2 - поиск по названию, 3 - поиск по году издания");
                        int k1 = scan.nextInt();
                        switch(k1){
                            case 1:
                                b = l.searchBook(typeValue("Автор книги: "),2);
                                break;
                            case 2:
                                b = l.searchBook(typeValue("Наименование книги"),1);
                                break;
                            case 3:
                                b = l.searchBook(Integer.parseInt(typeValue("Год издания")));
                                break;
                        }
                        continue;
                    case 3:
                        l.deleteBook(b);
                        b = null;
                        break;
                }
            }
        }
    }public static String typeValue(String type){
        Scanner scan = new Scanner(System.in);
        System.out.println(type);
        return scan.next();
    }
}
